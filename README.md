# Mastercard Test Api Client

The Mastercard provenance API allows recording and verification of supply chain events. These events are written to the Mastercard Blockchain guaranteeing an auditable, immutable ledger of supply chain events.


  For more information, please visit [https://www.mastercard.us/en-us/vision/who-we-are/innovation/provenance-solution.html](https://www.mastercard.us/en-us/vision/who-we-are/innovation/provenance-solution.html)


## This maven project demonstrates the calls to all Apis in pop application.

The main class is ApiClientTest.java. It contains: 
 1. configuration of the Mastercard service URL,
 2. configuration to load signing key,
 3. the interceptor code responsible for signing HTTP requests, and the tests for all APIs in pop application
    run against sandbox.

## Use the following commands to clean and build the maven project:
1) `mvn clean compile`
2) `mvn clean install`
3) `mvn clean verify`

## In order to see results of the tests, you will need to run ApiClientTest.java file located in Test_Api_Client/src/main/java.
Please follow the following steps to run the project:
1. cd into target directory : `cd /Test_Api_Client/target`
2. Run using the following command : `java -jar mastercard-provenance-dogfooding-1.0-SNAPSHOT-jar-with-dependencies.jar`

If any changes are made to any files, then make sure to run `mvn clean package` in Test_Api_Client(root) directory first




