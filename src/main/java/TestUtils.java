import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import com.acme.app.mastercard.ApiResponse;
import com.acme.app.mastercard.model.Aggregate;
import com.acme.app.mastercard.model.AggregateData;
import com.acme.app.mastercard.model.Aggregateable;
import com.acme.app.mastercard.model.DirectoryEntry;
import com.acme.app.mastercard.model.Document;
import com.acme.app.mastercard.model.DocumentAssociatedItems;
import com.acme.app.mastercard.model.DocumentAssociatedItemsDocumentIdentities;
import com.acme.app.mastercard.model.DocumentIdentity;
import com.acme.app.mastercard.model.Geofence;
import com.acme.app.mastercard.model.GeofenceProperties;
import com.acme.app.mastercard.model.GlobalOrganizationID;
import com.acme.app.mastercard.model.Invoice;
import com.acme.app.mastercard.model.Location;
import com.acme.app.mastercard.model.LogisticalUnitId;
import com.acme.app.mastercard.model.Metadata;
import com.acme.app.mastercard.model.MpgsMerchantAccount;
import com.acme.app.mastercard.model.Observation;
import com.acme.app.mastercard.model.Organization;
import com.acme.app.mastercard.model.PaymentTrigger;
import com.acme.app.mastercard.model.PaymentTriggerCondition;
import com.acme.app.mastercard.model.Point;
import com.acme.app.mastercard.model.Product;
import com.acme.app.mastercard.model.ProductClassification;
import com.acme.app.mastercard.model.ProductIdentity;
import com.acme.app.mastercard.model.ProvenanceEvent;
import com.acme.app.mastercard.model.Rate;
import com.acme.app.mastercard.model.RateCriteria;
import com.acme.app.mastercard.model.Shipment;
import com.acme.app.mastercard.model.ShipmentCharge;
import com.acme.app.mastercard.model.ShipmentDetail;
import com.acme.app.mastercard.model.Supplier;
import com.acme.app.mastercard.model.SupplierPaymentGatewayAccount;
import com.acme.app.mastercard.model.SupplyChainData;
import com.acme.app.mastercard.model.Transform;
import com.acme.app.mastercard.model.TransformData;
import com.acme.app.mastercard.model.Webhook;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;

public class TestUtils {
    public static final String DOCUMENT_ID1 = "25300000000000017";
    public static final String DOCUMENT_HASH = "6ba10c6a34dab200e9ec71bfbfddb652beff9930e5027534af70e18f99219907";
    public static final String BLOCKCHAIN_HASH = "a6EMajTasgDp7HG/v922Ur7/mTDlAnU0r3Dhj5khmQc=";
    public static final String DOCUMENT_TYPE = "BillOfLading";
    public static final String DOCUMENT_SUBTYPE = "Fair Trade";
    public static final Instant DOCUMENT_TIME_NOW = LocalDateTime.now().toInstant(ZoneOffset.UTC);
    public static final String TIME_HOURS_AGO= LocalDateTime.now().minusHours(3).toInstant(ZoneOffset.UTC).toString();
    public static final String USER_ID = "0000000000013";
    public static final String STATUS_DRAFT = "draft";
    public static final String GTIN = "05000267099028";
    public static final String LOT_NUMBER = "AA1234444";
    public static final String SERIAL_NUMBER = "123456789101";
    public static final String TRANSFORM_DATA_TYPE = TestUtils.EventDataType.TRANSFORM.getDescription();
    public static final String SENDER_GLN = "0500026709902";
    public static final String HASH = "7465737420686173680d0a";
    private static final String SIGNATURE = "7465737420686173680d0a";
    public static final String DOCUMENT_ID2 = "25300000000000018";
    public static final String GLOBAL_ID_TYPE = "LEI";
    public static final String GLOBAL_ID_VALUE = RandomStringUtils.randomAlphanumeric(10);
    public static final String GLOBAL_ID_VALUE2 = RandomStringUtils.randomAlphanumeric(10);
    public static final String GLOBAL_ID_VALUE3 = RandomStringUtils.randomAlphanumeric(10);
    public static final String DEVICE_ID = "4dac812e-7ad3-45ae-896d-8b5465f74b07";
    public static final String ORGANIZATION_NAME = "Organization name";
    public static final String ORGANIZATION_GLN1 = "0500026709903";
    public static final String ORGANIZATION_GLN2 = "0500026709903";
    public static final String ORGANIZATION_GTIN1 = "230909000081";
    public static final String ORGANIZATION_GTIN2 = "039487300000";
    public static String ORGANIZATION_ID_VALUE = "MC84SDXS4OIPKPCJUB16";
    public static String SUPPLIER_ORGANIZATION_ID_VALUE = "MC84SDXS4OIPKPCJUB16";
    public static String BUYER_ORGANIZATION_ID_VALUE = "MC84SDXS4OIPKPCJUB16";
    public static final String ORGANIZATION_ID_VALUE_2 = "MC84SDXS5OIPKPCJUB30";
    public static final String ORGANIZATION_ID_TYPE = "LEI";
    private static final String PRODUCT_CLASSIFICATION_REF = ProductClassificationReference.BEC.getDescription();
    public static final String CLASSIFICATION_REF = ProductClassificationReference.BEC.getDescription();
    public static final String CLASSIFICATION_CODE = "10623";
    private static final String GEOFENCE_TYPE = "Feature";
    private static final float LATITUDE = 27.111f;
    private static final float LONGITUDE = 70.111f;
    private static final List<Float> POINT_COORDINATES = Arrays.asList(LATITUDE, LONGITUDE);
    private static final String GEOFENCE_SUBTYPE = "Circle";
    private static final String POINT_GEOMETRY_TYPE = "Point";
    private static final Float CIRCLE_RADIUS = 500.01f;
    public static final String AGGREGATE_DATA_TYPE = EventDataType.AGGREGATE.getDescription();
    public static final String SSCC = "012345678901234567";
    public static final String TIME_NOW = LocalDateTime.now().toInstant(ZoneOffset.UTC).toString();
    public static final String GLN = RandomStringUtils.randomAlphanumeric(13);
    public static final String TEST_ACCESS_TOKEN = "NzI0YTcxOWQtNWU4YS00MDI1LTliNDktNTZjM2NkMmRmNzc2";
    public static final String TEST_URL = "http://localhost:8080/pop_sandbox/notifications"; // http://localhost:51991/notifications
    public static final String NOTIFICATION_RAND0M_PORT_TEST_URL = "http://localhost:%d/notifications";
    public static final Instant now = Instant.now();
    public static final String effectiveDate = now.minus(5, ChronoUnit.DAYS).toString();
    public static final String expiryDate = now.plus(5, ChronoUnit.DAYS).toString();
    public static final String INVALID_ENUM_VALUE = "Invalid value for property 'status': '%s', must be one of: %s";


    public enum EventDataType {
    COMMISSION("CommissionData"),
    CONSUME("ConsumeData"),
    TRANSFORM("TransformData"),
    SEND("SendData"),
    RECEIVE("ReceiveData"),
    AGGREGATE("AggregateData"),
    DISAGGREGATE("DisaggregateData"),
    OBSERVATION_ALERT("ObservationAlertData"),
    DAMAGE("DamageData"),
    DEPART("DepartData"),
    ARRIVE("ArriveData"),
    TRANSFER("TransferData");

    private final String description;

    EventDataType(String description) {
        this.description = description;
    }

    private static final Map<String, EventDataType> lookupMap =  Maps.uniqueIndex(
            Arrays.asList(EventDataType.values()), EventDataType::getDescription
    );

    public String getDescription() {
        return description;
    }

    public static EventDataType fromDescription(String description) {
        EventDataType type = lookupMap.get(description);
        if (type == null) {
            throw new IllegalArgumentException("Unknown event type description: "+description);
        }
        return type;
    }
}

    public enum RateType {
        @JsonProperty("PerShipment")
        PER_SHIPMENT("PerShipment"),
        @JsonProperty("Multiplier")
        MULTIPLIER("Multiplier"),
        @JsonProperty("Weight")
        WEIGHT("Weight"),
        @JsonProperty("DimWeight")
        DIM_WEIGHT("DimWeight");

        RateType(String description) {
            this.description = description;
        }

        private static final Map<String, RateType> lookupMap;

        static {
            lookupMap = new HashMap<>(values().length);
            for (RateType s : values()) {
                lookupMap.put(s.getDescription(), s);
            }
        }

        private final String description;

        public String getDescription() {
            return description;
        }

        public static RateType fromDescription(final String type) {
            RateType rateType = lookupMap.get(type);
            if (rateType == null) {
                throw new IllegalArgumentException("Unknown Rate type: " + type);
            }
            return rateType;
        }
    }

    public enum InvoiceStatus {
        DRAFT("Draft"), FINAL("Final"), CANCELLED("Cancelled");

        private final String description;
        private static final Map<String, InvoiceStatus> lookupMap;

        InvoiceStatus(String description) {
            this.description = description;
        }

        static {
            lookupMap = new HashMap<>(values().length);
            for (InvoiceStatus s : values()) {
                lookupMap.put(s.getDescription(), s);
            }
        }

        public String getDescription() {
            return description;
        }

        @SuppressWarnings("UnusedReturnValue")
        public static InvoiceStatus fromDescription(String status) {
            InvoiceStatus invoiceStatus = lookupMap.get(status);
            if (invoiceStatus == null) {
                throw new IllegalArgumentException(format(INVALID_ENUM_VALUE, status, getAllFields().toString()));
            }
            return invoiceStatus;
        }

        private static List<String> getAllFields() {
            return Arrays.stream(InvoiceStatus.values())
                    .map(InvoiceStatus::getDescription)
                    .collect(Collectors.toList());
        }

        // Jackson serialises using InvoiceStatus description instead of name
        @SuppressWarnings("unused")
        @JsonValue
        public String jsonValue() {
            return description;
        }
    }

    public enum WebhookStatus {
        ACTIVE("active"),
        INACTIVE("inactive"),
        CANCELLED("cancelled");

        private final String description;

        WebhookStatus(String description) {
            this.description = description;
        }

        private static final Map<String, WebhookStatus> lookupMap = Maps.uniqueIndex(
                Arrays.asList(WebhookStatus.values()), WebhookStatus::getDescription
        );

        public String getDescription() {
            return description;
        }

        public static WebhookStatus fromDescription(String description) {
            WebhookStatus type = lookupMap.get(description);
            if (type == null) {
                throw new IllegalArgumentException("Unknown webhook status description: "+description);
            }
            return type;
        }
    }

    public enum ProductClassificationReference {
        HS("HS"),
        BEC("BEC"),
        SITC("SITC");

        private final String description;

        ProductClassificationReference(String description) {
            this.description = description;
        }

        @SuppressWarnings("ConstantConditions")
        private static final Map<String, ProductClassificationReference> lookupMap = Maps.uniqueIndex(
                Arrays.asList(ProductClassificationReference.values()), ProductClassificationReference::getDescription
        );

        public String getDescription() {
            return description;
        }

        @SuppressWarnings("UnusedReturnValue")
        public static ProductClassificationReference fromDescription(String description) {
            ProductClassificationReference type = lookupMap.get(description);
            if (type == null) {
                throw new IllegalArgumentException("Unknown product classification reference: "+description);
            }
            return type;
        }
    }

    public static Document mockDocument() {
        return new Document()
                .documentId(DOCUMENT_ID1)
                .documentHash(DOCUMENT_HASH)
                .documentType("BillOfLading")
                .status(STATUS_DRAFT)
                .userId(USER_ID)
                .documentSubType(DOCUMENT_SUBTYPE)
                .documentTime(DOCUMENT_TIME_NOW.toString())
                .associatedItems(ASSOCIATED_ITEMS);
    }

    public static TransformData testTransformData() {
        return (TransformData) new TransformData()
                .transform(testTransform())
                .dataType(TRANSFORM_DATA_TYPE)
                .metadata(testMetadata())
                .additionalMetadata(testAdditionalMetadata())
                .userId(SENDER_GLN);
    }

    public static final DocumentAssociatedItems ASSOCIATED_ITEMS = new DocumentAssociatedItems()
            .addLogisticalUnitIdentitiesItem(
                    new LogisticalUnitId()
                            .gtin("00000000000014")
                            .sscc("000000000000000001")
            )
            .addDocumentIdentitiesItem(
                    new DocumentAssociatedItemsDocumentIdentities()
                            .documentId(DOCUMENT_ID2)
                            .documentType("BillOfLading")
            )
            .addOrganizationIdentitiesItem(
                    new GlobalOrganizationID()
                            .identifierType(GLOBAL_ID_TYPE)
                            .identifierValue(GLOBAL_ID_VALUE));

    private static Transform testTransform() {
        List<ProductIdentity> productIdentities = new ArrayList<>();
        productIdentities.add(productIdentity("123456789102"));
        productIdentities.add(productIdentity("123456789103"));
        return new Transform().inputItems(productIdentities).outputItem(testProductIdentity());
    }

    private static ProductIdentity productIdentity(final String serialNumber) {
        return new ProductIdentity()
                .gtin(GTIN)
                .serialNumber(serialNumber)
                .lotNumber(LOT_NUMBER);
    }

    public static ProductIdentity testProductIdentity() {
        return productIdentity(SERIAL_NUMBER);
    }

    public static Map<String, String> testAdditionalMetadata() {
        Map<String, String> additionalMetadata = new HashMap<>();
        additionalMetadata.put("DistributionId", "2356928750927y876563t67EA");
        additionalMetadata.put("DistributionChannelId", "938745932-EA");
        return additionalMetadata;
    }

    public static Metadata testMetadata() {
        return new Metadata().secondaryReferenceIdentifier("9b8e70110e68f4e46adf9e45851af6ea0e878d1f544b583e2e53b91cdd777d1d");
    }

    public static ProvenanceEvent testProvenanceEvent(final SupplyChainData event) {
        return testProvenanceEvent(HASH, event);
    }

    public static ProvenanceEvent testProvenanceEvent(final String hash, final SupplyChainData event) {
        return provenanceEvent(hash, getRandomEventAddress(), event);
    }

    private static ProvenanceEvent provenanceEvent(final String hash, final String address, final SupplyChainData supplyChainData) {
        return new ProvenanceEvent()
                .address(address)
                .hash(hash)
                .signature(SIGNATURE)
                .eventData(supplyChainData);
    }

    public static String getRandomEventAddress() {
        return RandomStringUtils.randomNumeric(10);
    }


        public static Observation mockObservation() {
        String observationTime = LocalDateTime.now().minusMinutes(15).toInstant(ZoneOffset.UTC).toString();
        return new Observation()
                .gtin(GTIN)
                .serialNumber(SERIAL_NUMBER)
                .deviceId(DEVICE_ID)
                .userId(SENDER_GLN)
                .time(observationTime);
        }

    public static Organization testOrganizationWithWithLocationsAndProducts() {
        Organization data = new Organization().organizationName(ORGANIZATION_NAME)
                .globalOrganizationID(new GlobalOrganizationID()
                        .identifierValue(RandomStringUtils.randomAlphanumeric(10))
                        .identifierType(ORGANIZATION_ID_TYPE));
        data.addProductsItem(testProduct(RandomStringUtils.randomAlphanumeric(12)));
        data.addLocationsItem(testLocation(RandomStringUtils.randomAlphanumeric(13)));
        return data;
    }

    public static Organization testOrganization() {
        return new Organization()
                .organizationName(ORGANIZATION_NAME)
                .globalOrganizationID(new GlobalOrganizationID()
                        .identifierValue(ORGANIZATION_ID_VALUE)
                        .identifierType(ORGANIZATION_ID_TYPE));
    }

    public static Product testProduct(String gtin) {
        return new Product().gtin(gtin)
                .classification(new ProductClassification()
                        .classificationReference(CLASSIFICATION_REF)
                        .classificationCode(CLASSIFICATION_CODE));
    }
    public static Location testLocation(String gln) {
        return new Location().globalLocationNumber(gln)
                .geofence(
                        testPointGeofence());
    }
    public static Geofence testPointGeofence() {
        return new Geofence()
                .type(GEOFENCE_TYPE)
                .geometry(new Point().coordinates(POINT_COORDINATES).type(POINT_GEOMETRY_TYPE))
                .properties(testGeofenceProperties());
    }

    private static GeofenceProperties testGeofenceProperties() {
        return new GeofenceProperties().subtype(GEOFENCE_SUBTYPE).radius(CIRCLE_RADIUS);
    }

    public enum AccountType {
        BOOST, BOOST_COF, MPGS, ICCP_VCN
    }

    public static MpgsMerchantAccount testMpgsMerchantAccount(GlobalOrganizationID globalOrganizationID) {
        MpgsMerchantAccount mpgsAccount = new MpgsMerchantAccount();
        mpgsAccount.setType(AccountType.MPGS.name());
        mpgsAccount.setOrganizationId(globalOrganizationID);
        mpgsAccount.setRegion("TEST");
        mpgsAccount.setMerchantId("TESTPROVENANCE");
        return mpgsAccount;
    }
    public static AggregateData testAggregateData() {
        return aggregateData(defaultAggregate());
    }
    private static AggregateData aggregateData(Aggregate aggregate) {
        return (AggregateData) new AggregateData()
                .aggregate(aggregate)
                .dataType(AGGREGATE_DATA_TYPE)
                .eventTime((TIME_NOW))
                .userId(SENDER_GLN);
    }

    private static Aggregate defaultAggregate() {
        List<Aggregateable> ssscItems = new ArrayList<>();
        ssscItems.add(new Aggregateable().serialNumber(SERIAL_NUMBER).gtin(GTIN));

        return new Aggregate()
                .sscc(SSCC).gtin(GTIN)
                .items(ssscItems);
    }

    public static final String PEM_EXCLUDING_GUARDS = "MIICGTCCAcCgAwIBAgIIPZWB7YQ/19MwCgYIKoZIzj0EAwIwgYQxCzAJBgNVBAYT" +
            "AlVTMREwDwYDVQQIEwhTdCBMb3VpczETMBEGA1UEChMKbWFzdGVyY2FyZDETMBEG" +
            "A1UECxMKbWFzdGVyY2FyZDEUMBIGA1UEAxMLbWFzdGVyY2FyZDExIjAgBgkqhkiG" +
            "9w0BCQEWE21hc3RlcmNhcmRAbWFpbC5jb20wHhcNMTkwNjE0MTQzNDAwWhcNMjAw" +
            "NjE0MTQzNDAwWjCBhDELMAkGA1UEBhMCVVMxETAPBgNVBAgTCFN0IExvdWlzMRMw" +
            "EQYDVQQKEwptYXN0ZXJjYXJkMRMwEQYDVQQLEwptYXN0ZXJjYXJkMRQwEgYDVQQD" +
            "EwttYXN0ZXJjYXJkMTEiMCAGCSqGSIb3DQEJARYTbWFzdGVyY2FyZEBtYWlsLmNv" +
            "bTBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABK2B8x+rn9cup7KXpZzumc2nmceg" +
            "WJl5D6ehb6dkxbiKWh2jO0stm+kkMFSb7bf1WoHBywuQTL5z8JnlS7+hfTajGjAY" +
            "MAkGA1UdEwQCMAAwCwYDVR0PBAQDAgTQMAoGCCqGSM49BAMCA0cAMEQCIDliVZBp" +
            "7uj6R6emHSndR5Xh4Et7smEh+fUpUXfD34fzAiAb5tUcI32L9hJKysNDzCKDZrx2" +
            "N1nKdl23u5SPGfwEXA==";

    public static DirectoryEntry testCertificate(){
        return new DirectoryEntry()
                .certificate(PEM_EXCLUDING_GUARDS)
                .globalLocationNumber(GLN)
                .gtin(GTIN);
    }
    public static Organization testOrganization(final String name) {
        ORGANIZATION_ID_VALUE = RandomStringUtils.randomAlphanumeric(10);
        return new Organization()
                .organizationName(name)
                .globalOrganizationID(new GlobalOrganizationID()
                        .identifierValue(ORGANIZATION_ID_VALUE)
                        .identifierType(ORGANIZATION_ID_TYPE));
    }

    public static Organization testBuyerOrganization(final String name) {
        BUYER_ORGANIZATION_ID_VALUE = RandomStringUtils.randomAlphanumeric(10);
        Location location = new Location().globalLocationNumber(GLN);

        List<Location> locations = new ArrayList<>();
        locations.add(location);
        return new Organization()
                .organizationName(name)
                .globalOrganizationID(new GlobalOrganizationID()
                        .identifierValue(BUYER_ORGANIZATION_ID_VALUE)
                        .identifierType(ORGANIZATION_ID_TYPE))
                .locations(locations);
    }

    public static Organization testSupplierOrganization(final String name) {
        SUPPLIER_ORGANIZATION_ID_VALUE = RandomStringUtils.randomAlphanumeric(10);
        return new Organization()
                .organizationName(name)
                .globalOrganizationID(new GlobalOrganizationID()
                        .identifierValue(SUPPLIER_ORGANIZATION_ID_VALUE)
                        .identifierType(ORGANIZATION_ID_TYPE));
    }

    public static PaymentTriggerCondition testPaymentTriggerCondition() {
        return new PaymentTriggerCondition()
                .events(newArrayList("ABCDEFabcdef" + RandomStringUtils.random(10, false, true)))
                .documentIdentities(newArrayList(testDocumentIdentity(RandomStringUtils.random(10, false, true))));
    }

    public static DocumentIdentity testDocumentIdentity(final String documentId) {
        return new DocumentIdentity().documentId(documentId).documentType("PurchaseOrder").documentStatus(STATUS_DRAFT).userId(SENDER_GLN);
    }

    public static PaymentTrigger testMpgsPaymentTrigger() {

        return new PaymentTrigger()
                .orderId(RandomStringUtils.randomAlphanumeric(10))
                .amount(BigDecimal.valueOf(Math.random()).setScale(2, RoundingMode.CEILING))
                .currency("USD")
                .buyerOrganizationId(
                        new GlobalOrganizationID()
                                .identifierType("LEI")
                                .identifierValue(BUYER_ORGANIZATION_ID_VALUE))
                .supplierOrganizationId(
                        new GlobalOrganizationID()
                                .identifierType("LEI")
                                .identifierValue(SUPPLIER_ORGANIZATION_ID_VALUE))
                .paymentMethod("MPGS")
                .sourceOfFunding("MPGS_COF")
                .auth(testPaymentTriggerCondition())
                .pay(testPaymentTriggerCondition())
                .voidAuthorization(testPaymentTriggerCondition())
                .invoiceNumber(RandomStringUtils.randomAlphanumeric(10));
    }

    public static Webhook testWebhook(final String url, final String accessToken) {
        return new Webhook().url(url)
                .accessToken(accessToken)
                .status(WebhookStatus.ACTIVE.getDescription());
    }

    public static Supplier testMpgsSupplier(String identifierType, String identifierValue) {
        Supplier provenanceSupplier = new Supplier();
        provenanceSupplier.setOrganizationId(new GlobalOrganizationID()
                .identifierType(identifierType)
                .identifierValue(identifierValue));
        provenanceSupplier.setAccount(new SupplierPaymentGatewayAccount()
                .region("TEST")
                .merchantId("TESTPROVENANCE")
                .type("MPGS"));
        return provenanceSupplier;
    }

    public static Shipment testShipmentWithCharges(GlobalOrganizationID carrierOrganization, GlobalOrganizationID customerOrganization) {
        Shipment shipment = testShipment(carrierOrganization, customerOrganization);
        List<ShipmentCharge> shipmentCharges = new ArrayList<>();
        shipmentCharges.add(new ShipmentCharge().description("Govt Charges").name("GAT Charge").amount(new BigDecimal("36.40").setScale(2, BigDecimal.ROUND_UP)));
        shipmentCharges.add(new ShipmentCharge().description("Admin Charge").name("Admin").amount(new BigDecimal("22.00").setScale(2, BigDecimal.ROUND_UP)));
        shipment.setCharges(shipmentCharges);
        return shipment;
    }

    public static Shipment testShipment(GlobalOrganizationID carrierOrganization, GlobalOrganizationID customerOrganization) {
        List<ShipmentDetail> shipmentDetails = new ArrayList<>();
        shipmentDetails.add(new ShipmentDetail().name("origin").value("Dublin"));
        shipmentDetails.add(new ShipmentDetail().name("destination").value("New York"));
        shipmentDetails.add(new ShipmentDetail().name("serviceLevel").value("Standard"));
        return new Shipment().carrierOrganizationId(carrierOrganization)
                .customerOrganizationId(customerOrganization)
                .associatedSsccs(Collections.singletonList("12345"))
                .globalShipmentIdentifier("1Z54F78A0450293517")
                .status("Active");
    }

    public static Rate testPerShipmentRate(GlobalOrganizationID carrierOrganization, GlobalOrganizationID customerOrganization) {
        return testPerShipmentRate(carrierOrganization, customerOrganization, effectiveDate, expiryDate);
    }

    public static Rate testPerShipmentRate(GlobalOrganizationID carrierOrganization, GlobalOrganizationID customerOrganization,
                                           String effectiveDate, String expiryDate) {
        return new Rate()
                .amount(BigDecimal.TEN.setScale(2))
                .name("Per Shipment")
                .description("A Per Shipment rate")
                .type(RateType.PER_SHIPMENT.getDescription())
                .currency("USD")
                .carrierOrganizationId(carrierOrganization)
                .customerOrganizationId(customerOrganization)
                .effectiveDate(effectiveDate)
                .expiryDate(expiryDate)
                .criteria(Arrays.asList(
                        new RateCriteria().name("origin").value("Dublin"),
                        new RateCriteria().name("destination").value("New York"),
                        new RateCriteria().name("serviceLevel").value("Standard")));
    }

    public static Invoice testInvoice(UUID shipmentId, Integer version) {
        Invoice invoice = new Invoice();
        invoice.setInvoiceNumber("abc-123");
        invoice.setCurrency("USD");
        invoice.setShipmentId(shipmentId);
        invoice.setShipmentVersion(version);
        invoice.setTotal(new BigDecimal("100.00"));
        invoice.setStatus(InvoiceStatus.DRAFT.getDescription());
        return invoice;
    }

    public static UUID getResourceIdFromCreateResponse(ApiResponse<Void> response) {
        String location = response.getHeaders().get("Location").get(0);
        try {
            UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUri(new URI(location));
            UriComponents uriComponents = uriComponentsBuilder.build();
            List<String> segments = uriComponents.getPathSegments();
            return UUID.fromString(segments.get(segments.size() - 1));
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(format("Location is invalid: %s", location));
        }
    }
    }


