import com.acme.app.mastercard.ApiClient;
import com.acme.app.mastercard.ApiException;
import com.acme.app.mastercard.ApiResponse;
import com.acme.app.mastercard.api.AccountsApi;
import com.acme.app.mastercard.api.CertificatesApi;
import com.acme.app.mastercard.api.DocumentsApi;
import com.acme.app.mastercard.api.EventBatchApi;
import com.acme.app.mastercard.api.EventsApi;
import com.acme.app.mastercard.api.InvoicesApi;
import com.acme.app.mastercard.api.ObservationsApi;
import com.acme.app.mastercard.api.OrganizationApi;
import com.acme.app.mastercard.api.PaymentsApi;
import com.acme.app.mastercard.api.RatesApi;
import com.acme.app.mastercard.api.ShipmentsApi;
import com.acme.app.mastercard.api.WebhooksApi;
import com.acme.app.mastercard.model.GlobalOrganizationID;
import com.acme.app.mastercard.model.Organization;
import com.acme.app.mastercard.model.PaymentTokenOrganizations;
import com.acme.app.mastercard.model.PaymentTrigger;
import com.acme.app.mastercard.model.ProvenanceEvent;
import com.acme.app.mastercard.model.ProvenanceEventBatch;
import com.acme.app.mastercard.model.ProvenanceEventBatchDetails;
import com.acme.app.mastercard.model.ProvenanceOrganizationId;
import com.acme.app.mastercard.model.Shipment;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mastercard.developer.interceptors.OkHttpOAuth1Interceptor;
import com.mastercard.developer.utils.AuthenticationUtils;
import okhttp3.OkHttpClient;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ApiClientTest {
    public static ApiResponse<Void> documentResponse2;
    public static ApiResponse<Void> eventResponse2;
    public static ApiResponse<ProvenanceEventBatchDetails> batchResponse2;
    public static ApiResponse<Void> observationResponse2;
    public static ApiResponse<ProvenanceOrganizationId> organizationResponse2;
    public static ApiResponse<Void> certificatesResponse2;
    public static ApiResponse<Void> shipmentResponse2;
    public static ApiResponse<Void> ratesResponse2;
    public static ApiResponse<Void> invoicesResponse2;
    public static ProvenanceEvent transformEvent2 = TestUtils.testProvenanceEvent(TestUtils.testTransformData().userId(null));
    public static ProvenanceEvent aggregateEvent2 = TestUtils.testProvenanceEvent(TestUtils.testAggregateData().userId(null));
    public static List<ProvenanceEvent> eventList2 = Arrays.asList(transformEvent2, aggregateEvent2);
    public static final ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    public static void main(String[] args) throws UnrecoverableKeyException, CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, ApiException {

        ApiClient client = new ApiClient();
        OkHttpClient.Builder httpClientBuilder = client.getHttpClient().newBuilder();

// Configure the Mastercard service URL
//        client.setBasePath("http://localhost:8080/pop_sandbox"); // to run locally
        client.setBasePath("https://api.mastercard.com/pop"); // to run against sandbox

// Load the signing key
        PrivateKey signingKey = null;
        try (InputStream is = ApiClientTest.class.getResourceAsStream("keyalias-production.p12")) {
            signingKey = AuthenticationUtils.loadSigningKey(is, "keyalias", "keystorepassword123");
        } catch (RuntimeException e) {
            throw e;
        }
// Add the interceptor code responsible for signing HTTP requests
        httpClientBuilder.addInterceptor(new OkHttpOAuth1Interceptor("E3b3OB-NBwX3IyTxK6HLu6Khb1wjC2cJYgT1lMr79ce0b1d2!5bf8f6872cfd4df392f11d37fe922ac50000000000000000", signingKey));
// ...
        client.setHttpClient(httpClientBuilder.build());

//        DocumentsApi
        DocumentsApi documentsApi = new DocumentsApi(client);
        ApiResponse<Void> documentResponse = documentsApi.postDocumentWithHttpInfo(TestUtils.mockDocument());

        System.out.println("Posting a Document to the Provenance /documents endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(TestUtils.mockDocument()));
        System.out.println("Post status code: " + documentResponse.getStatusCode());

//        EventsApi
        EventsApi eventsApi = new EventsApi(client);
        ProvenanceEvent provenanceEvent = TestUtils.testProvenanceEvent(TestUtils.testTransformData());
        ApiResponse<Void> eventResponse = eventsApi.postEventWithHttpInfo(provenanceEvent);
        System.out.println("Posting a Provenance Event to the Provenance /events endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(provenanceEvent));
        System.out.println("Post status code: " + eventResponse.getStatusCode());

//        EventBatchApi
        EventBatchApi eventBatchApi = new EventBatchApi(client);
        ProvenanceEvent transformEvent = TestUtils.testProvenanceEvent(TestUtils.testTransformData());
        ProvenanceEvent aggregateEvent = TestUtils.testProvenanceEvent(TestUtils.testAggregateData());
        final List<ProvenanceEvent> eventList = Arrays.asList(transformEvent, aggregateEvent);

        ProvenanceEventBatch batch = new ProvenanceEventBatch().events(eventList);
        ApiResponse<ProvenanceEventBatchDetails> eventBatchResponse = eventBatchApi.postEventBatchWithHttpInfo(batch);
        System.out.println("Posting a Provenance Event Batch to the Provenance /batches/events endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(eventList));
        System.out.println("Post status code: " + eventBatchResponse.getStatusCode());

//        ObservationsApi
        ObservationsApi observationsApi = new ObservationsApi(client);
        ApiResponse<Void> observationResponse = observationsApi.postObservationWithHttpInfo(TestUtils.mockObservation());
        System.out.println("Posting an Observation to the Provenance /observations endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(TestUtils.mockObservation()));
        System.out.println("Post status code: " + observationResponse.getStatusCode());

//        OrganizationApi
        OrganizationApi organizationApi = new OrganizationApi(client);
        ApiResponse<ProvenanceOrganizationId> organizationResponse = organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts());
        System.out.println("Posting an Organization to the Provenance /organizations endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(TestUtils.testOrganizationWithWithLocationsAndProducts()));
        System.out.println("Post status code: " + organizationResponse.getStatusCode());

//        Certificates Api
        CertificatesApi certificatesApi = new CertificatesApi(client);
        ApiResponse<Void> certificateResponse = certificatesApi.postCertificateWithHttpInfo(TestUtils.testCertificate());
        System.out.println("Posting a Certificate to the Provenance /certificates endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(TestUtils.testCertificate()));
        System.out.println("Post status code: " + certificateResponse.getStatusCode());

//        ShipmentsApi
        ShipmentsApi shipmentsApi = new ShipmentsApi(client);
        GlobalOrganizationID shipmentGlobalOrganizationID = new GlobalOrganizationID().identifierType(TestUtils.GLOBAL_ID_TYPE).identifierValue(TestUtils.GLOBAL_ID_VALUE2);
        GlobalOrganizationID shipmentGlobalOrganizationID2 = new GlobalOrganizationID().identifierType(TestUtils.GLOBAL_ID_TYPE).identifierValue(TestUtils.GLOBAL_ID_VALUE3);
        organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(shipmentGlobalOrganizationID));
        organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(shipmentGlobalOrganizationID2));
        ApiResponse<Void> shipmentResponse = shipmentsApi.createShipmentWithHttpInfo(TestUtils.testShipment(shipmentGlobalOrganizationID,shipmentGlobalOrganizationID2));
        System.out.println("Posting a Shipment to the Provenance /shipments endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(TestUtils.testShipment(shipmentGlobalOrganizationID,shipmentGlobalOrganizationID2)));
        System.out.println("Post status code: " + shipmentResponse.getStatusCode());

//        Rates
        RatesApi ratesApi = new RatesApi(client);
        GlobalOrganizationID rateGlobalOrganizationID = new GlobalOrganizationID().identifierType(TestUtils.GLOBAL_ID_TYPE).identifierValue(RandomStringUtils.randomAlphanumeric(10));
        GlobalOrganizationID rateGlobalOrganizationID2 = new GlobalOrganizationID().identifierType(TestUtils.GLOBAL_ID_TYPE).identifierValue(RandomStringUtils.randomAlphanumeric(10));
        organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(rateGlobalOrganizationID));
        organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(rateGlobalOrganizationID2));
        ApiResponse<Void> ratesResponse = ratesApi.createRateWithHttpInfo(TestUtils.testPerShipmentRate(rateGlobalOrganizationID,rateGlobalOrganizationID2));
        System.out.println("Posting a Rate to the Provenance /rate endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(TestUtils.testPerShipmentRate(rateGlobalOrganizationID,rateGlobalOrganizationID2)));
        System.out.println("Post status code: " + ratesResponse.getStatusCode());

//        Invoices
        InvoicesApi invoicesApi = new InvoicesApi(client);
        GlobalOrganizationID rateGlobalOrganizationID3 = new GlobalOrganizationID().identifierType(TestUtils.GLOBAL_ID_TYPE).identifierValue(RandomStringUtils.randomAlphanumeric(10));
        GlobalOrganizationID rateGlobalOrganizationID4 = new GlobalOrganizationID().identifierType(TestUtils.GLOBAL_ID_TYPE).identifierValue(RandomStringUtils.randomAlphanumeric(10));
        organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(rateGlobalOrganizationID3));
        organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(rateGlobalOrganizationID4));
        ApiResponse<Void> shipmentPostResponse = shipmentsApi.createShipmentWithHttpInfo(TestUtils.testShipment(rateGlobalOrganizationID3,rateGlobalOrganizationID4));
        UUID shipmentId = TestUtils.getResourceIdFromCreateResponse(shipmentPostResponse);
        UUID shipmentResourceid = TestUtils.getResourceIdFromCreateResponse(shipmentPostResponse);
        ApiResponse<Shipment> shipResponse = shipmentsApi.getShipmentWithHttpInfo(shipmentResourceid);
        Integer vnum = shipResponse.getData().getVersionNumber();

        ApiResponse<Void> invoiceResponse = invoicesApi.createInvoiceWithHttpInfo(TestUtils.testInvoice(shipmentId,vnum));
        System.out.println("Posting an Invoice to the Provenance /freight-invoices endpoint");
        System.out.println("Request payload: ");
        System.out.println(ow.writeValueAsString(TestUtils.testShipment(rateGlobalOrganizationID3,rateGlobalOrganizationID4)));
        System.out.println("Post status code: " + invoiceResponse.getStatusCode());

//        Negative test for documentsApi
        try {
            // Posting an invalid document
            documentResponse2 = documentsApi.postDocumentWithHttpInfo(TestUtils.mockDocument().documentType(""));

        } catch (ApiException e) {
            System.out.println("Posting an invalid Document to the Provenance /documents endpoint returns Bad Request");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.mockDocument().documentType("blah")));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for eventsApi
        try {
            // Posting an invalid event
            ProvenanceEvent provenanceEvent2 = TestUtils.testProvenanceEvent(TestUtils.testTransformData().userId(null));
            eventResponse2 = eventsApi.postEventWithHttpInfo(provenanceEvent2);

        } catch (ApiException e) {
            System.out.println("Posting an invalid Provenance Event to the Provenance /events endpoint returns bad request");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.testTransformData().userId(null)));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for eventBatchesApi
        try {
            // Posting an invalid event batch
                batchResponse2 = eventBatchApi.postEventBatchWithHttpInfo(batch.events(eventList2));

        } catch (ApiException e) {
            System.out.println("Posting an invalid Provenance Event Batch to the Provenance /batches/events endpoint");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(eventList2));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for observationsApi
        try {
            // Posting an invalid observation
            observationResponse2 = observationsApi.postObservationWithHttpInfo(TestUtils.mockObservation().gtin(null).lotNumber(null).serialNumber(null));

        } catch (ApiException e) {
            System.out.println("Posting an invalid Provenance observation to the Provenance /observations endpoint");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.mockObservation().gtin(null).lotNumber(null).serialNumber(null)));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for organizationsApi
        try {
            // Posting an invalid organization
            organizationResponse2 = organizationApi.postOrganizationWithHttpInfo(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(null));

        } catch (ApiException e) {
            System.out.println("Posting an invalid Provenance organization to the Provenance /organizations endpoint");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.testOrganizationWithWithLocationsAndProducts().globalOrganizationID(null)));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for certificatesApi
        try {
            // Posting an invalid certificate
            certificatesResponse2 = certificatesApi.postCertificateWithHttpInfo(TestUtils.testCertificate().globalLocationNumber(null).organizationId(null).gtin(null));

        } catch (ApiException e) {
            System.out.println("Posting an invalid Provenance certificate to the Provenance /certificates endpoint");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.testCertificate().globalLocationNumber(null).organizationId(null).gtin(null)));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for shipmentsApi
        try {
            // Posting an invalid shipment
            shipmentResponse2 = shipmentsApi.createShipmentWithHttpInfo(TestUtils.testShipment(null,null));

        } catch (ApiException e) {
            System.out.println("Posting an invalid shipment to the Provenance /shipments endpoint");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.testShipment(null,null)));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for ratesApi
        try {
            // Posting an invalid rate
            ratesResponse2 = ratesApi.createRateWithHttpInfo(TestUtils.testPerShipmentRate(null,null));

        } catch (ApiException e) {
            System.out.println("Posting a Rate to the Provenance /rate endpoint");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.testPerShipmentRate(null,null)));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }

//        Negative test for invoicesApi
        try {
            // Posting an invalid rate
            invoicesResponse2 = invoicesApi.createInvoiceWithHttpInfo(TestUtils.testInvoice(null,null));

        } catch (ApiException e) {
            System.out.println("Posting an Invoice to the Provenance /freight-invoices endpoint");
            System.out.println("Request payload: ");
            System.out.println(ow.writeValueAsString(TestUtils.testInvoice(null,null)));
            System.out.println("Response body with bad request error: ");
            System.out.println(e.getResponseBody());
        }
    }
}




